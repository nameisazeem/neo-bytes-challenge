FROM ubuntu:22.04
 
COPY my_first_script.sh /
RUN chmod +x /my_first_script.sh && /my_first_script.sh
 
CMD ["Finished!"]
